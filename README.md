## 1. TS基本类型

- TS是js的超集，包含js**基础**的全部类型
- 基础类型：Boolean、Number、String、null、undefined以及ES6的Symbol和ES10的Bignt

#### 1.1 字符串类型

- 字符串使用`string`定义的

```typescript
let a: string = '123'
// 普通声明

// es6的字符串模板
let str: string = `ikun${a}`
```

#### 1.2 数字类型

支持十六进制、十进制、八进制和二进制

```typescript
let notANumber: number = NaN;//Nan
let num: number = 123;//普通数字
let infinityNumber: number = Infinity;//无穷大
let decimal: number = 6;//十进制
let hex: number = 0xf00d;//十六进制
let binary: number = 0b1010;//二进制
let octal: number = 0o744;//八进制s
```

#### 1.3 布尔类型

==注意，使用构造函数Boolean创造的对象不是布尔值==

```typescript
let createdBoolean: boolean = new Boolean(1)
//这样会报错 应为事实上 new Boolean() 返回的是一个 Boolean 对象 
```

事实上 new Boolean() 返回的是一个 Boolean 对象 需要改成

```typescript
let createdBoolean: Boolean = new Boolean(1)
```

```typescript
let booleand: boolean = true //可以直接使用布尔值
 
let booleand2: boolean = Boolean(1) //也可以通过函数返回布尔值
```

#### 1.4 空置类型

JavaScript没用空值（Void）的概念，在typescript中，可以用void表示没用任何返回值的函数

```typescript
function voidFn():void {
    console.log('test void')
}
```

void类型的用法，主要是用在我们**不希望**调用者关心函数返回值情况下，比如通常的**异步回调函数**

**void也是可以定义undefined和null类型**

```typescript
let u: void = undefined
let n: void = null
```

#### 1.5 Null和undefined类型

```typescript
let u: undefined = undefined;//定义undefined
let n: null = null;//定义null
```

**void 和 undefined 和 null 最大的区别**

与 void 的区别是，undefined 和 null 是所有类型的子类型。也就是说 undefined 类型的变量，可以赋值给 string 类型的变量：

```typescript
//这样写会报错 void类型不可以分给其他类型
let test: void = undefined
let num2: string = "1"
 
num2 = test
```

```typescript
//这样是没问题的
let test: null = null
let num2: string = "1"
 
num2 = test
 
//或者这样的
let test: undefined = undefined
let num2: string = "1"
 
num2 = test
```

> tips: 
>
> 如果你配置了tsconfig.json 开启了严格模式
>
> ```json
> {
>     "compilerOptions":{
>         "strict": true
>     }
> }
> ```
>
> ![image-20230901114244504](TypeScripimage/image-20230901114244504.png)
>
> ==null 不能 赋予void类型==

## 2. Any类型和unknown顶级类型

1. 没有限定那种类型，随时切换类型都可以，能对any类型进行任何操作，不需要类型检查

```typescript
let anys:any = 123
anys = '123'
anys = true
```

2. 声明变量的时候没有指定任意类型默认为any

```typescript
let anys
angys = 123
anys = '123'
```

3. 弊端：如果使用了any就失去了TS类型检测的作用了
4. TypeScript 3.0中引入的 unknown 类型也被认为是 top type ，但它更安全。与 any 一样，所有类型都可以分配给unknown

**unknow**：unknown类型比any更加严格

```typescript
//unknown 可以定义任何类型的值
let value: unknown;
 
value = true;             // OK
value = 42;               // OK
value = "Hello World";    // OK
value = [];               // OK
value = {};               // OK
value = null;             // OK
value = undefined;        // OK
value = Symbol("type");   // OK
 
//这样写会报错unknow类型不能作为子类型只能作为父类型 any可以作为父类型和子类型
//unknown类型不能赋值给其他类型
let names:unknown = '123'
let names2:string = names
 
//这样就没问题 any类型是可以的
let names:any = '123'
let names2:string = names   
 
//unknown可赋值对象只有unknown 和 any
let bbb:unknown = '123'
let aaa:any= '456'
 
aaa = bbb
```

区别2

```typescript
如果是any类型在对象没有这个属性的时候还在获取是不会报错的
let obj:any = {b:1}
obj.a
 
 
如果是unknow 是不能调用属性和方法
let obj:unknown = {b:1,ccc:():number=>213}
obj.b
obj.ccc()
```

## 3. 对象的类型

在typescript中，我们定义对象的方式要用关键字`interface`（接口），我的理解是使用`interface`来定义一种约束，让数据的结构满足约束的格式。

如：

```typescript
// 定义一个对象约束
interface Person {
    b: string,
    a: string
}

// 使用定义的约束
// 不能像下面这样写，必须与定义接口保持一致，不能多属性也不能少属性
const person:Preson = {
    a: '123'
}
```

- 可以重名`interface`，重名的`interface`可以合并

```typescript
interface A{name:string}
interface A{age:number}
var x:A={name:'xx',age:20}
//继承
interface A{
    name:string
}
 
interface B extends A{
    age:number
}
 
let obj:B = {
    age:18,
    name:"string"
}
```

## 4. 可选属性 使用？操作符

```typescript
// 可选属性的含义是该属性可以不存在
interface Person {
    b?: string,
    a: string
}

const person:Person = {
    a: '123'
}
```

## 5. 任意属性[propName:string]

==需要注意的是，一旦定义了容易属性，那么确定属性和可选属性的；类型都必须是它的子集：==

- [propName:string]里面的propName可以是随便什么，只要不怕被打死aa bb cc都行

```typescript
//在这个例子当中我们看到接口中并没有定义C但是并没有报错
//应为我们定义了[propName: string]: any;
//允许添加新的任意属性
interface Person {
    b?:string,
    a:string,
    [propName: string]: any;
}
 
const person:Person  = {
    a:"213",
    c:"123"
}
```

## 6. 只读属性 readonly

readon 只读属性是不允许被赋值只能读取

```typescript
//这样写是会报错的
//应为a是只读的不允许重新赋值
interface Person {
    b?: string,
    readonly a: string,
    [propName: string]: any;
}
 
const person: Person = {
    a: "213",
    c: "123"
}
 
person.a = 123
```

## 7. 添加函数

```typescript
interface Person {
    b?: string,
    readonly a: string,
    [propName: string]: any,
    cd:()=>void
}

const person: Person = {
    a: '321',
    c: '456',
    cd: ()=>{
       	console.log(123)
    }
}
```

## 8. 数组的类型

-`类型[]`

```typescript
//类型加中括号
let arr:number[] = [123]
//这样会报错定义了数字类型出现字符串是不允许的
let arr:number[] = [1,2,3,'1']
//操作方法添加也是不允许的
let arr:number[] = [1,2,3,]
arr.unshift('1')
 
 
var arr: number[] = [1, 2, 3]; //数字类型的数组
var arr2: string[] = ["1", "2"]; //字符串类型的数组
var arr3: any[] = [1, "2", true]; //任意类型的数组
```

## 9. 数组的泛型

> **tips:**
>
> 什么是泛型呢？
> 泛型简单来说就是类型变量，在ts中存在类型，如number、string、boolean等。泛型就是使用一个类型变量来表示一种类型，类型值通常是在使用的时候才会设置。泛型的使用场景非常多，可以在函数、类、interface接口中使用

- `规则Array<类型>`

```typescript
let arr:Array<number> = [1,2,3,4]
```

## 10. 用接口表示数字

一般用来藐视数组

```typescript
interface NumberArray {
    [index: number]: number;
}
let fibonacci: NumberArray = [1, 1, 2, 3, 5];
//表示：只要索引的类型是数字时，那么值的类型必须是数字。
```

## 11. 多维数组

```typescript
let data: number[][] = [[1,2],[3,4]]
```

## 12. arguments类数组

> tips:
>
> 在 TypeScript 中，与 JavaScript 一样，`arguments` 对象也是一个类数组对象，它包含了传递给函数的所有参数。然而，在 TypeScript 中，对 `arguments` 对象的类型定义需要特别处理，以确保类型安全性。

```typescript
function Arr(...args:any): void {
    console.log(arguments)
    //错误的arguments 是类数组不能这样定义
    let arr:number[] = arguments
}
Arr(111, 222, 333)
 
 
 
function Arr(...args:any): void {
    console.log(arguments) 
    //ts内置对象IArguments 定义
    let arr:IArguments = arguments
}
Arr(111, 222, 333)
 
//其中 IArguments 是 TypeScript 中定义好了的类型，它实际上就是：
interface IArguments {
[index: number]: any;
length: number;
callee: Function;
}
```

## 13. any在数组中的应用

一个常见的例子数组中可以存在任意类型

```typescript
let list: any[] = ['test',1,[],{a:1}]
```

